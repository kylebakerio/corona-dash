var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const routerFor = {
  // index: require('./routes/index'),
  overview: require('./routes/global-overview'),
  compare: require('./routes/compare'),
  dash: require('./routes/dashboard.js'),
  news: require('./routes/news.js'),
  charts: require('./routes/charts.js'),
  analysis: require('./routes/analysis.js'),
  table: require('./routes/maintablepage.js'),
  thoughts: require('./routes/thoughts.js'),
  about: require('./routes/about.js'),
  api: require('./routes/api').router,
};

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'surge-payload-base')));

// actually used
app.use('/analysis', routerFor.analysis);
app.use('/about', routerFor.about); // 
app.use('/compare', routerFor.compare);

// not actually used
app.use('/dash', routerFor.dash);
app.use('/api', routerFor.api);
app.use('/news', routerFor.news);
app.use('/table', routerFor.table);
app.use('/charts', routerFor.charts);
app.use('/thoughts', routerFor.thoughts);

// app.use('/', routerFor.analysis);
app.get('/', (req, res) => {
  res.redirect('/analysis');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
