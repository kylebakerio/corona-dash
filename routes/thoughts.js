var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render(`thoughts`, { data: {} });
});

module.exports = router;
