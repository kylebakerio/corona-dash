var express = require('express');
var router = express.Router();
const surgify = require('../utils/surgify').surgify;


const apiDict = {
  "hopkins": require("../api-handlers/hopkins.js").getHopkins,
  "worldometer": require("../api-handlers/worldometer.js").getWorldometer,
};

const lastUpdate = {
  worldometer: "1/1/1969",
};

const checkForUpdateAfter = {
  worldometer: new Date("1/1/1969"),
};

const featFlag = {
  genTemplates: true,
  deployToSurge: true,
};

let setUpdateLimits = () => apiDict["worldometer"](featFlag.genTemplates, featFlag.deployToSurge).then(data => {
  // NOTE
  // this logic is closely mirror on the client side in utils.ejs
  // in order to prevent unnecessary server pings
  // if this is updated, mirror it there.
  if (!data) {
    throw new Error("no data when setting update limit?", data)
  }
  lastUpdate.worldometer = data.World.meta.lastDate;
  console.log('worldometer lastupdate set:', lastUpdate)
  
  checkForUpdateAfter['worldometer'] = new Date(lastUpdate.worldometer);
  checkForUpdateAfter['worldometer'].setDate(checkForUpdateAfter['worldometer'].getDate() + 2);
  checkForUpdateAfter['worldometer'].setHours(0,0,0,0);
  console.log('will check for update upon:', checkForUpdateAfter['worldometer']);
  console.log(`currently`, new Date());
})
.catch(e => {
  console.error(e);
  console.error("failed to set update limits, trying again in 10 minutes");
  setTimeout(setUpdateLimits, 1000 * 60 * 10);
});

setUpdateLimits();

let deploying = true;
const callSurge = (data) => {
  if (!featFlag.genTemplates) return;
  deploying = true;
  surgify(data, featFlag.deployToSurge)
  .then(() => {
    deploying = false;
  })
  .catch(e => {
    console.error("error deploying to surge, trying again", e)
    callSurge()
  })
};

apiDict.worldometer()
.then(callSurge);

router.get('/', async function(req, res, next) {
  const stats = await apiDict.worldometer();
  const lastDate = stats.World.meta.lastDate;
  res.send(`<html><body><pre>last pull from ${req.params.api} api:</br></br>${JSON.stringify(stats, " ", 4)}</pre></body><script>var all=${JSON.stringify(stats, " ", 4)};</script></html>`);
  // res.send(`<html><body><pre>last pull from hopkins api:</br></br>${JSON.stringify(stats, " ", 4)}</pre></body></html>`);
});

router.get('/:api', async function(req, res, next) {
  const stats = await apiDict[req.params.api]();
  const lastDate = req.params.api === "worldometer" ? stats.World.meta.lastDate : "";
  res.send(`<html><body><pre>last pull from ${req.params.api} api was ${lastDate}:</br></br>${JSON.stringify(stats, " ", 4)}</pre></body><script>var all=${JSON.stringify(stats, " ", 4)};</script></html>`);
});

router.get('/getnew/:api', async function(req, res, next) {
  // if no update or last update was recent enough, returns 'null'
  // if worth trying update, checks; if new data, returns it, and starts a deploy; otherwise returns null
  let api = req.params.api;
  if (new Date() < checkForUpdateAfter[api]) {
    let hours = Math.abs(checkForUpdateAfter[api] - (new Date())) / 36e5;
    console.log(`it's currently \n ${new Date()} \n and we won't check for updates until we pass \n${checkForUpdateAfter[api]}\n (in ${Math.round(hours)} hours)`)
    res.send(null); // not sure if we can do this? we'll see.
  }
  else if (deploying) {
    console.warn("already in the middle of a deploy, so exiting here.")
    res.send(null) // could choose to send data here as well... could cause multiple annoying popups for a minute or so while deploy is going on and user navigates around site, though.
    // on the other hand, sending null means only one user gets this notification.
    // in reality, we should be able to send the data and actually live update with it.
    // otherwise, we should record whether the notification has shown, and only show it once per day.
    // TODO
  }
  else {
    const data = await apiDict[api]();
    const freshestUpdate = data.World.meta.lastDate;
    if (new Date(freshestUpdate) > new Date(lastUpdate.worldometer)) {
      console.log(`checked for new data; \n${new Date(freshestUpdate)}\n is more recent than \n${new Date(lastUpdate.worldometer)}\n so we'll send fresh data`);
      console.warn("\n\nDEPLOYING TO SURGE\n\n")
      callSurge(data);
      res.send(data); // currently this data isn't utilized by the client as anything other than a flag to notify the user, but in future updates could be used to dynamically update.
      // instead, calling usrgify causes a re-deploy of the static site, meaning they'll get the new data 
    } else {
      console.log(`checked for new data, but \n${new Date(freshestUpdate)}\n is not more recent than \n${new Date(lastUpdate.worldometer)}\n so we cannot send fresh data`);
      res.send(null);
    }
  }
});

router.get('/force/:api', async function(req, res, next) {
  // if no update or last update was recent enough, returns 'null'
  // if worth trying update, checks; if new data, returns it, and starts a deploy; otherwise returns null
  let api = req.params.api;
  const stats = await apiDict[api]();
  const freshestUpdate = stats.World.meta.lastDate;
  res.send(stats);
});

exports.router = router;
