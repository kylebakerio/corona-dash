var express = require('express');
var router = express.Router();
const getCovidData = require('../api-handlers/worldometer').getWorldometer

/* 

const fs = require('fs');
const { exec } = require("child_process");


*/

console.warn("\n\nNEED TO MOVE THIS LOGIC TO CLIENT SIDE\n\n")

router.get('/', function(req, res, next) {
    getCovidData(req).then(data => {
        res.render(`analysis`, { data, region:"" });
    })
});

router.get('/:region', function(req, res, next) {
    let region = req.params.region || "";

    getCovidData(req).then(data => {
        if (region) {
            let regions = Object.keys(data)
            if (regions.includes(region)) {
                // console.log('has exact region', region)
                // good, region is appriately named
            }
            else if (regions.includes(region.toLowerCase()[0].toUpperCase()+region.toLowerCase().slice(1))) {
                // needed to uppercase first letter, like most countries
                region = region.toLowerCase()[0].toUpperCase()+region.toLowerCase().slice(1);
                // console.log('has uppercased(1) region', region)
            }
            else if (regions.includes(region.toUpperCase())) {
                // needed to fully uppercase, eg USA
                region = region.toUpperCase();
                // console.log('has fully upercased region', region)
            }
            // else? well, we tried. Client will give it a shot.
        }
        console.log('region is', region)
        res.render(`analysis`, { data, region });
    })
});

module.exports = router;
