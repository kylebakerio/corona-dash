var express = require('express');
var router = express.Router();
const getCovidData = require('../api-handlers/worldometer').getWorldometer

router.get('/', function(req, res, next) {
    // res.redirect('/analysis');
    getCovidData(req).then(data => {
        res.render(`global-overview`, { data });
    })
});

module.exports = router;
