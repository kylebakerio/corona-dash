var express = require('express');
var router = express.Router();
const getCovidData = require('../api-handlers/worldometer').getWorldometer

router.get('/', function(req, res, next) {
    getCovidData(req).then(data => {
        // GO SOMEWHERE NEW
        res.render(`global-overview`, { data });
    })
});

module.exports = router;
