var express = require('express');
var router = express.Router();

/* GET dashboards. */
router.get('/:num', function(req, res, next) {
    let num = req.params.num;
    console.warn(`REQUEST FOR A DASH: ${num}`)
    res.render(`backup/dash${num}`, { title: 'Express' });
});

module.exports = router;
