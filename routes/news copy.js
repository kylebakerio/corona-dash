var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render(`a filename in views`, { title: 'Express' });
});

module.exports = router;
