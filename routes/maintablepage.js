var express = require('express');
var router = express.Router();
const getCovidData = require('../api-handlers/hopkins').getHopkins

router.get('/', function(req, res, next) {
  getCovidData(req).then(data => {
    res.render(`maintablepage`, { data });
  })
});

module.exports = router;
