const express = require('express');

const fs = require('fs');
const { /* exec,  */spawn } = require("child_process");
const {
    email: surgeEmail,
    password: surgePassword,
    url: surgeURL,
} = require('./surge-creds').surge

// note that whatever is in index 0 will be duplicated as 'index' as well
let activePages = ['analysis', 'compare', 'about'];

let renderPage = (hasApp, pageName, pageTemplate, data) => new Promise((resolve, reject) => {
    console.log("rendering", pageName)
    hasApp.app.render(pageTemplate, { data, region:"" }, (err, renderedPage) => {
        fs.writeFile(`surge-payload/${pageName}.html`, renderedPage, (err) => {
            if (err) throw err;
            else {
                // success case, the file was saved
                console.log('Render successful!', pageName);
                resolve()
            }
        });
    })
});


let surgify = async function(data, deployToSurge) {
    return new Promise((resolve, reject) => {
        
        const app = require("../app"); // cannot be at top of file, creates circular dependency
        const hasApp = { app };

        console.warn("Will render static files");

        new Promise((resolve, reject) => {
            console.log("spawning rm process")
            const deleteSurgePayload = spawn("rm", ["-rf", "surge-payload"])
            
            deleteSurgePayload.stdout.on("data", console.log.bind(null, "while rm:"));
            deleteSurgePayload.stderr.on("data", data => {
                console.error(`stderr while rm: ${data}`);
                throw new Error("error while deleting surge payload", data)
            });

            deleteSurgePayload.on('error', (error) => {
                console.error(`error while rm: ${error.message}`);
                throw new Error("error while deleting surge payload", data)
            });

            deleteSurgePayload.on("close", code => {
                console.log(`RESOLVING | rm old surge payload process exited with code ${code}`);
                resolve();
            });
        }).then(
            () => new Promise((resolve, reject) => {
                console.log("spawning copy process")
                const makeSurgePayload = spawn("cp", ["-r", "surge-payload-base", "surge-payload"])
                makeSurgePayload.stdout.on("data", console.log.bind(null, "while copying"));
                makeSurgePayload.stderr.on("data", data => {
                    console.error(`stderr while copying: ${data}`);
                    throw new Error("error while making surge payload", data)
                });

                makeSurgePayload.on('error', (error) => {
                    console.error(`error while copying: ${error.message}`);
                    throw new Error("error while making surge payload", data)
                });

                makeSurgePayload.on("close", code => {
                    console.log(`copying to make surge payload process exited with code ${code}`);
                    resolve();
                });
            })
        )
        .then( () => 
            Promise.all(
                activePages.map((page, i) => {
                    let renderPromises = [];
                    if (i === 0) renderPromises.push(renderPage(hasApp, '200', page, data))
                    renderPromises.push(renderPage(hasApp, page, page, data))
                    return Promise.all(renderPromises)
                })
            )
        )
        .then(() => {
            if (!deployToSurge) {
                console.warn("will not deploy to surge")
                return
            }
            console.warn("will deploy to surge")
            // TODO:
            // IF PROD, ...
            
            // old non streaming output way:
            // exec("surge surge-payload surgify-covid-isjsdhas.surge.sh", (error, stdout, stderr) => {
            //     if (error) {
            //         console.log(`error: ${error.message}`);
            //         return;
            //     }
            //     if (stderr) {
            //         console.log(`stderr: ${stderr}`);
            //         return;
            //     }
            //     console.log(`stdout: ${stdout}`);
            // });
            console.log("spawning surge")
            const surge = spawn("surge"/*, ["surge-payload" , surgeURL ]*/)

            surge.stdout.on("data", data => {
                console.log(`surge:\n${data}`);
                let passwordAttempts = 0;
                if (data.includes("email:")) {// not yet functional, needs to be exact
                    surge.stdin.write(`${surgeEmail}\n`);
                    console.log(`input:\n ${data} ${surgeEmail}\n`);
                } 
                else if(data.includes("password:") && passwordAttempts === 0) {
                    passwordAttempts++;
                    surge.stdin.write(`${surgePassword}\n`);
                    console.log(`input:\n${data} ${"[surge password here]"}\n`);
                }
                else if (data.includes('domain:')) {
                    surge.stdin.write(`${surgeURL}\n`);
                    console.log(`input:\n${data} ${surgeURL}\n`);
                }
                else if (data.includes('project:')) {
                    surge.stdin.write(`${'surge-payload'}\n`);
                    console.log(`input:\n${data} ${'surge-payload'}\n`);
                }
                else if (data.includes('forgot?') || passwordAttempts > 0) {
                    console.error(`ERROR, wrong password?\n ${data}`);
                    throw new Error('error deploying to surge, seems like we have the wrong password');
                }
                else if (data.includes('success!') || passwordAttempts > 0) {
                    // console.log(`input:\n${data} `);
                    resolve();
                }
                else {
                    // can turn on for debugging, but produces a lot of bugger garbage
                    // console.log("no response prepared:\n", data);
                }
            });

            surge.stderr.on("data", data => {
                console.error(`stderr: ${data}`);
            });

            surge.on('error', (error) => {
                console.error(`error: ${error.message}`);
                throw new Error('surge error', error);
            });

            surge.on("close", code => {
                console.log(`surge process exited with code ${code}`);
                resolve()
            });

        })
        .then(resolve)
        .catch(console.error)
    });
}
    
exports.surgify = surgify;
