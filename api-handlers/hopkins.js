var unirest = require("unirest");

let lastApiQueryTime;
let lastHopkins;

const getHopkins = function() {
    return new Promise(resolve => {

        if (!!lastApiQueryTime && (new Date() - lastApiQueryTime) < 1800000) {
            console.log("using cached api")
            resolve(normalizeHopkins(lastHopkins)) // if less than [30 minutes in milliseconds], use cached response.
        }
        else { // else, start the ping to update 
            console.log("refreshing cached api in background")

            // source: https://rapidapi.com/KishCom/api/covid-19-coronavirus-statistics/endpoints
            var req = unirest("GET", "https://covid-19-coronavirus-statistics.p.rapidapi.com/v1/stats");

            req.query({
            //     "country": "US" // this is from the api documentation, but doesn't seem to work.
                                   // We'll do this on our end anyways, so doesn't matter.
            });

            req.headers({
                "x-rapidapi-host": "covid-19-coronavirus-statistics.p.rapidapi.com",
                "x-rapidapi-key": "ba6a6422bbmshbbd1a68ecf83adep1faa58jsn70de5baeda75" // this is out in the open... don't be evil, please. It's a free api, just go get your own key.
            });
            
            let delayedResolve = true;
            if ((new Date() - lastApiQueryTime) < 3600000) { // recent enough, less than an hour out of date, resolve with slightly stale data to improve response time
                resolve(normalizeHopkins(lastHopkins))
                let delayedResolve = false;
            }

            req.end(function (res) {
                if (res.error) {
                    // this can happen if the API call fails, for example. In that case, return the cached response.
                    console.error(res.error);
                    if (delayedResolve) resolve({status:res.error,world:{}}) // data was too old, so we have waited for fresh data.
                    
                }
                else {
                    lastHopkins = res.body;
                    lastApiQueryTime = new Date(); // reset timer
                    console.log("got fresh api response")
                    if (delayedResolve) resolve(normalizeHopkins(lastHopkins)) // data was too old, so we have waited for fresh data.
                }
            });
        }

    })
}

const normalizeHopkins = function(rawHopkins) {
    if (!rawHopkins) {
        console.log(rawHopkins, 'there was a failure')
        return {world:{hopkins}, status:"error, could not normalize"}
    }
    let normalized = {
        world: rawHopkins.data.covid19Stats,
    };

    let collapseCountries = normalized.world.reduce((memo, datum) => { if (datum.city || datum.province) memo[datum.country] = 0; return memo }, {})
    collapseCountries = Object.keys(collapseCountries);

    // collapseCountries = ["Australia", "Denmark", "China", "Canada", "France", "Netherlands", "United Kingdom", "US"];

    collapseCountries.forEach(country => {
        let countryDetailedData = normalized.world.filter(datum => datum.country === country);
        normalized.world = normalized.world.filter(datum => datum.country !== country);
        let countryCollapsedData = countryDetailedData.reduce((memo, datum) => {
            memo.confirmed += datum.confirmed;
            memo.deaths += datum.deaths;
            memo.recovered += datum.recovered;
            return memo;
            },
            {country, confirmed:0, deaths:0, recovered:0}
        );
        normalized.world.push(countryCollapsedData);
        normalized[country] = countryDetailedData;
    });

    // console.log(normalized, normalized.US, normalized.world["US"])
    // console.log('normalied:',normalized, '/n normalied^');
    return normalized
};

getHopkins(); // run once on launch to cache api response in advance

exports.getHopkins = getHopkins;
// exports.lastHopkins = lastHopkins; // sync, for easier 

// async how-to from MDN
// function resolveAfter2Seconds() {
// return new Promise(resolve => {
//     setTimeout(() => {
//     resolve('resolved');
//     }, 2000);
// });
// }

// async function asyncCall() {
//     console.log('calling');
//     const result = await resolveAfter2Seconds();
//     console.log(result);
//     // expected output: 'resolved'
// }

// asyncCall();
// end from MDN
