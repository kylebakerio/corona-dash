var unirest = require("unirest");
const csv=require('csvtojson')

let gitPages = ["Confirmed", "Deaths", "Recovered"];
let rawGitBase = page => `https://raw.githubusercontent.com/bumbeishvili/covid19-daily-data/master/time_series_19-covid-${page}.csv`;
const surgify = require('../utils/surgify').surgify;

let lastApiQueryTime;
let cachedWorldometer3CSV = {};
let cachedWorldometerJSON = {};
let priorCachedLastDate = "1/1/1969";
let fetching = null; // either a promise for a result, or null.

const getWorldometer = function(generateStatic, deployToSurge) {
    if (fetching) return fetching;
    let request = new Promise((innerResolve, reject) => {
        let resolve = (data) => {
            fetching = null;
            innerResolve(data);
        };
        if (Object.keys(cachedWorldometerJSON).length && !!lastApiQueryTime && (new Date() - lastApiQueryTime) < (1800000)) {
            console.log("using cached api");
            resolve(normalizeWorldometer(cachedWorldometer3CSV)); // if less than [30 minutes in milliseconds], use cached response.
        }
        else { // else, start the ping to update 
            console.log("refreshing cached api");

            if (Object.keys(cachedWorldometerJSON).length) {
                console.log("but will send old data for now.")
                // resolve(normalizeWorldometer(cachedWorldometer3CSV)); // This is what we've been using, but let's try:
                resolve(new Promise(res => res(cachedWorldometerJSON)))
            }
            else {
                console.log("no old data, must be a new bootup, will wait to resolve until we've received data")
            }
            
            request3CSV()
            .then(values => {
                // values should be the same as cachedWorldometer3CSV, which we also just set statefully as a variable...

                // let willNormalize = normalizeWorldometer(cachedWorldometer3CSV) // how we've normally done it, but will try this:
                let willNormalize = normalizeWorldometer(values)
                
                // needs to be called directly, instead.
                //
                // if (generateStatic) {
                //     console.warn("will evaluate whether to generate static with new data when normalized")
                //     willNormalize.then(data => {
                //         console.log('date comparison:', new Date(data.World.meta.lastDate), new Date(priorCachedLastDate))
                //         if (!initializedTemplates || new Date(data.World.meta.lastDate) > new Date(priorCachedLastDate)) {
                //             initializedTemplates = true;
                //             console.log('will run surgify', initializedTemplates, new Date(data.World.meta.lastDate), new Date(priorCachedLastDate));
                //             surgify(data, deployToSurge);
                //         }
                //         else {
                //             console.log('will not run surgify');
                //         }
                //     }) // update static pages and deploy to surge
                // }
                // else console.warn("will not generate static with new data")

                
                // resolve with promise
                resolve(willNormalize)
            })
            .catch(error => console.error('probably not connected to internet--couldn\'t get worldometers api output', error))
        }
    })
    .catch(err => {
        console.error(`got \n\n ${err} \n\n on first load, was not able to get new or cached data, going to setTimeout and try again in 5 seconds; are you connected to the internet? Is the API source down?`)
        if (Object.keys(cachedWorldometerJSON).length) {
            // console.log(cachedWorldometerJSON, "this^ was old data about to return")
            console.error("\n\ntrying to return old data because of error getting new data\n\n")
            return cachedWorldometerJSON
        }
        else {
            console.error("\n\nthere was no old data (did we just start the server? aborting)\n\n")
            setTimeout(getWorldometer, 5000)
        }
    });
    fetching = request;
    return request;
};

const request3CSV = () => new Promise(finalResolve => {
    // source: https://github.com/bumbeishvili/covid19-daily-data/
    // we make 3 requests here, need to wait for all requests to finish.
    let csvRequests = gitPages.map(page => {
        console.log(`trying: ${rawGitBase(page)}`);
        
        return new Promise((resolve, reject) => {
            var req = unirest("GET", rawGitBase(page));
            req.query({});
            req.headers({});
            req.end(function(response) {
                if (response.error) {
                    // console.error("problem, wasn't able to update worldometer api, trying to use old data...", response.error);
                    // resolve(cachedWorldometer3CSV[page])
                    reject("problem, wasn't able to update worldometer api, trying to use old data...", response.error)
                }
                // else
                // cachedWorldometer3CSV[page] = response.body;
                console.log("got fresh api response", page)
                // resolve(cachedWorldometer3CSV[page]) // data was too old, so we have waited for fresh data.
                resolve([page,response.body])
            });
        });
    });

    Promise.all(csvRequests)
    .then(values => {
        console.log("success, got all 3")
        lastApiQueryTime = new Date(); // reset timer
        
        // values
        values.forEach(tuple => {
            let page = tuple[0];
            let data = tuple[1];
            cachedWorldometer3CSV[page] = data;
        });

        finalResolve(cachedWorldometer3CSV);
    });
});

const normalizeWorldometer = async function(rawWorldometer) {
    // alternative stream method:
    // csv()
    // .fromStream(request.get('http://mywebsite.com/mycsvfile.csv'))
    // .subscribe((json)=>{
    //     return new Promise((resolve,reject)=>{
    //         // long operation for each json e.g. transform / write into database.
    //     })
    // },onError,onComplete);
    /**
    csvStr:
    1,2,3
    4,5,6
    7,8,9
    */
    let rawJSON = new Promise((resolve, reject) => {
        let all3 = gitPages.map(page => {
            if (!rawWorldometer[page]) {
                console.warn(`problem, rawWorldometer[${page}]`, rawWorldometer[page])
                reject("failure getting new data")
            }
            // else
            return csv()
            .fromString(rawWorldometer[page])
            .then(jsonObj => {
                let formatSingle = jsonObj.reduce((memo, countryObj) => {
                    let state = countryObj["Province/State"];
                    let country = countryObj["Country/Region"];

                    if (!memo[country]) {
                        memo[country] = countryObj;
                    }
                    if (state !== country) {
                        // seems this is irrelevant, that they all do...
                        console.error("We now have a state that doesn't match country. That's different. May want to check the code and make sure that won't cause a problem, previously we could assume they were always the same.")
                        memo[country].states = {};
                        memo[country].states[state] = countryObj;
                    }
                    Object.keys(memo[country]).forEach(date => memo[country][date] = Number(memo[country][date]))
                    return memo;
                }, {page: page});

                // console.log(formatSingle.Kosovo)

                return formatSingle;
            })
        });

        resolve(all3);
    });

    rawJSON = await rawJSON; 
    rawJSON = await rawJSON;
    // very, very bad code smell. Rushing through this.
    let preCombine = await Promise.all(rawJSON);

    newWorldometerJSON = {};
    Object.keys(preCombine[0]).forEach(country => newWorldometerJSON[country] = {});
    delete newWorldometerJSON.page;

    if (cachedWorldometerJSON.world) priorCachedLastDate = cachedWorldometerJSON.World.meta.lastDate;

    cachedWorldometerJSON = preCombine.reduce((memo, page) => {
        Object.keys(newWorldometerJSON).forEach(country => {
            newWorldometerJSON[country][page.page] = page[country];
            newWorldometerJSON[country].meta = {};
            
            // move non-dates to 'meta' child object
            Object.keys(newWorldometerJSON[country][page.page]).forEach(key => {
                debugger;
                if ( isNaN(Number(key[0]))) {
                    // console.log(`deleting ${key}`);
                    newWorldometerJSON[country].meta[key] = newWorldometerJSON[country][page.page][key];
                    delete newWorldometerJSON[country][page.page][key];
                    // console.log('new meta:', newSubCountryObject.meta[key])
                }
            })
        })
        return memo;
    }, newWorldometerJSON);

    let plusWorld = Object.keys(cachedWorldometerJSON).reduce((memo, country) => {
        
        gitPages.forEach(page => {
            // let mostRecent = Object.keys(country[page]).reduce((date1, date2) => {
            //     new Date(date1) > new Date(date2) ? date1 : date2;
            // })
            // mostRecent

            if (!memo[page]) memo[page] = {};
            // console.log('these', cachedWorldometerJSON,country,page)
            Object.keys(cachedWorldometerJSON[country][page]).forEach(date => {
                if (!memo[page][date]) memo[page][date] = 0;
                memo[page][date] += Number(cachedWorldometerJSON[country][page][date])
            });
        })

        return memo;
    }, {})
    
    cachedWorldometerJSON.World = plusWorld;
    
    let lastDate = Object.keys(cachedWorldometerJSON.World.Confirmed).reduce((date1, date2) => {
        return (date2 === "meta" ? date1 : (new Date(date1) > new Date(date2) ? date1 : date2))
    });
    
    cachedWorldometerJSON.World.meta = {
        lastDate: lastDate
    };
    
    cachedWorldometerJSON.World.meta.altNames = ["Globe", "All", "None"];
    cachedWorldometerJSON.World = plusWorld;
    cachedWorldometerJSON.USA.meta.altNames = ["United States", "US", "America"];
    cachedWorldometerJSON.UK.meta.altNames = ["United Kingdom"];
    cachedWorldometerJSON["Cote d'Ivoire"].meta.altNames = ["Côte d’Ivoire", "Ivory Coast"];
    

    return cachedWorldometerJSON;


    // let normalized = {
    //     world: rawWorldometer.data.covid19Stats,
    // };

    // let collapseCountries = normalized.world.reduce((memo, datum) => { if (datum.city || datum.province) memo[datum.country] = 0; return memo }, {})
    // collapseCountries = Object.keys(collapseCountries);

    // collapseCountries.forEach(country => {
    //     let countryDetailedData = normalized.world.filter(datum => datum.country === country);
    //     normalized.world = normalized.world.filter(datum => datum.country !== country);
    //     let countryCollapsedData = countryDetailedData.reduce((memo, datum) => {
    //         memo.confirmed += datum.confirmed;
    //         memo.deaths += datum.deaths;
    //         memo.recovered += datum.recovered;
    //         return memo;
    //         },
    //         {country, confirmed:0, deaths:0, recovered:0}
    //     );
    //     normalized.world.push(countryCollapsedData);
    //     normalized[country] = countryDetailedData;
    // });

    // return normalized
};

// getWorldometer() // run once on launch to cache api response in advance of first ping

exports.getWorldometer = getWorldometer;
